#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <queue>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

using namespace std;


class BorNode {
public:
	map<const char, BorNode *> links;
	BorNode *fail;
	BorNode *term;
	int out;

	BorNode(BorNode *fail_node = NULL)
		: fail(fail_node), term(NULL), out(-1)
	{}

	BorNode* getLink(const char c) const 
	{
		map<const char, BorNode *>::const_iterator iter = links.find(c);
		if (iter != links.cend()) {
			return iter->second;
		}
		else {
			return NULL;
		}
	}

	bool isTerminal() const 
	{
		return (out >= 0);
	}
};

class AhoCorasick
{
public:
	typedef void (*Callback) (const char* substr);
	BorNode root;
	vector<string> words;
	BorNode* current_state;

public:
	void addString(const char* const str) 
	{
		BorNode *current_node = &root;
		for(const char *cp = str; *cp; ++cp) {
			BorNode *child_node = current_node->getLink(*cp);
			if (!child_node) {
				child_node = new BorNode(&root);
				current_node->links[*cp] = child_node;
			}
			current_node = child_node;
		}
		current_node->out = words.size();
		words.push_back(str);
	}

	void init() 
	{
		queue<BorNode *> q;
		q.push(&root);
		while (!q.empty()) {
			BorNode *current_node = q.front();
			q.pop();
			for (map<const char, BorNode *>::const_iterator iter = current_node->links.cbegin();
				iter != current_node->links.cend(); ++iter)
			{
				const char symbol = iter->first;
				BorNode *child = iter->second;
				BorNode *temp_node = current_node->fail;
				while (temp_node) {
					BorNode *fail_candidate = temp_node->getLink(symbol);
					if (fail_candidate) {
						child->fail = fail_candidate;
						break;
					}
					temp_node = temp_node->fail;
				}
				if (child->fail->isTerminal()) {
					child->term = child->fail;
				}
				else {
					child->term = child->fail->term;
				}
				q.push(child);
			}
		}
	}

	void step(const char c) 
	{
		while (current_state) {
			BorNode *candidate = current_state->getLink(c);
			if (candidate) {
				current_state = candidate;
				return;
			}
			current_state = current_state->fail;
		}
		current_state = &root;
	}



	void search(const char* str) 
	{
		current_state = &root;
		for(int i = 0; *str; ++i, ++str) {
			step(*str);
			if (current_state->isTerminal()) {
				cout << i - words[current_state->out].size() << ": " << (words[current_state->out].c_str()) << endl;
			}
			BorNode *temp_node = current_state->term;
			while (temp_node) {
				cout << i - words[temp_node->out].size() << ": " << (words[temp_node->out].c_str()) << endl; 
				temp_node = temp_node->term;
			}
		}
	}
};

int main()
{
	AhoCorasick structured;
	int k;
	cin >> k;
	for (int i = 0; i < k; ++i) {
		string str;
		cin >> str;
		structured.addString(str.data());
	}
	structured.init();
	std::ifstream ifs;
	ifs.open("input.txt", std::ifstream::in);
	string text;
	while (!ifs.eof()) {
		string q;
		ifs >> q;
		text += " " + q;
	}
	
	structured.search(text.data());

	return 0;
}
