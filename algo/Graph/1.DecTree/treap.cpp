#include <iostream>
#include <ctime>
#include <cstdlib>

#define DEFINED 1000000
using namespace std;
struct item
{
	int key, prior;
	item *left, *right;
	item() { }
	item(int key, int prior) : key(key), prior(prior), left(NULL), right(NULL){ }
};

typedef item * ptree;

class Treap {
private:
	item *root;

public:
	Treap () :root(NULL)
	{}

	void insert (int k)
	{
		item * vertex = new item((k) % DEFINED, rand() % DEFINED);
		__insert__(root, vertex);
	}

	void erase(int key)
	{
		__erase__(root, key);
	}

	void print()
	{
		__print__(root);
	}
private:
	void __print__ (item * tree) {

		if (!tree)
		{
			return;
		}
		__print__(tree->left);
		std::cout << tree->key << " ";
		__print__(tree->right);
	}

	void __erase__(item * &tree, int key)
	{
		if (tree->key == key) {
			__merge__(tree, tree->left, tree->right);
		}
		else {
			if (key < tree->key) {
				__erase__(tree->left, key);
			}
			else {
				__erase__(tree->right, key);
			}
		}
	}
	void __insert__(item * &tree, item * vertex) {
		if (!tree) {
			tree = vertex;
			return;
		}

		if (vertex->key == tree->key) {
			return;
		}

		if (vertex->prior > tree->prior) {
			__split__(tree, vertex->key, vertex->left, vertex->right);
			tree = vertex;
		}
		else {
			if (vertex->key  < tree->key) {
				__insert__(tree->left, vertex);
			}
			else {
				__insert__(tree->right, vertex);
			}
		}

	}
	void __merge__(item * &tree, item * left, item * right) {
		if (!left) {
			tree = right;
			return;
		}

		if (!right) {
			tree = left;
			return;
		}

		if (left->prior > right->prior)
		{
			__merge__(left->right, left->right, right);
			tree = left;
		}
		else {
			__merge__(right->left, left, right->left);
			tree = right;
		}
	}
	void __split__(item * tree, int key, item * &left, item * &right)
	{
		if (!tree) {
			left = right = NULL;
			return;
		}

		if (key <= tree->key) {
			__split__(tree->left, key, left, tree->left);
			right = tree;
		} else {
			__split__(tree->right, key, tree->right, right);
			left = tree;
		}
	}

};


int main () {
	Treap it;
	it.insert(1);
	it.insert(2);

	it.insert(3);
	it.insert(4);
	it.insert(5);
	it.insert(6);
	it.print();
	cout << endl;
	it.erase(6);
	it.erase(1);
	it.print();
	cout << endl;
	return 0;
}

