#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <set>
#define INF INT_MAX
using namespace std;
class City {
public:
	int id;
	int x;
	int y;
	string name;
	City(int _x, int _y, string _name, int _id)
		:x(_x), y(_y), name(_name), id(_id)
	{}
};

class Edge {
public:
	int from;
	int to;
	double w;
	Edge (int _f, int _t, double _w)
		:from(_f), to(_t), w(_w)
	{}
};

bool operator < (const Edge f, const Edge other) {
	return f.w < other.w;
}
inline int area(City a, City b, City c)
{
	return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

bool intersect_1(int a, int b, int c, int d)
{
	if (a > b)
	{
		int buffer = a;
		a = b;
		b = buffer;
	}
	if (c > d)
	{
		int buffer = c;
		c = d;
		d = buffer;
	}
	return ((a > c) ? a : c) <= ((b < d) ? b : d);
}

bool intersect(City a, City b, City c, City d)
{
	return intersect_1(a.x, b.x, c.x, d.x)
		&& intersect_1(a.y, b.y, c.y, d.y)
		&& area(a, b, c) * area(a, b, d) <= 0
		&& area(c, d, a) * area(c, d, b) <= 0;
}

class Bar {
public:
	int xs, ys;
	int xe, ye;
	Bar (int _xs, int _ys, int _xe, int _ye) 
		:xs(_xs), ys(_ys), xe(_xe), ye(_ye)
	{}
};
class Graph {
public:
	int n;
	vector <City> cities;
	Graph (int N)
		:n(N) 
	{
		for (int i = 0; i < N; ++i) {
			int a, b;
			string name;
			cin >> name>> a >> b ;
			cities.push_back(City(a, b, name, i));
		}
	}

	void buildEdge (int m) {
		vector <Bar> bar;
		edges.resize(n);
		//for (int i = 0; i < n; ++i) edges[i].resize(n, 0);
		for (int i = 0; i < m; ++i) {
			int a, b,c, d;
			cin >>a>>b>>c>>d;
			bar.push_back(Bar(a, b,c,d));
		}
		for (int i = 0; i < n; ++i) {
			for (int j = i + 1; j < n; ++j) {
				__buildEdge__(cities[i], cities[j], bar);
			}
		}
	
	}
	void prim() {
		int included = 0;
		set<Edge> queue;
		vector<bool> isIncluded(n, false);
		isIncluded[0] = true;
		vector<string> roads;

		int currentVertex = 0;
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < edges[currentVertex].size(); ++j)
				queue.insert(edges[currentVertex][j]);

			if (queue.empty())
				break;
			set<Edge>::iterator it = queue.begin();

			while (isIncluded[it->to])
			{
				++it;
				if (it == queue.end())
					break;
			}
			if (it == queue.end())
				break;
			roads.push_back(cities[it->from].name);
			roads.push_back(cities[it->to].name);
			isIncluded[it->to] = true;
			currentVertex = it->to;
			++it;
			queue.erase(queue.begin(), it);
		}

		bool solutionExists = true;
		for (int i = 0; i < n; ++i)
		{
			if (!isIncluded[i])
			{
				solutionExists = false;
				break;
			}
		}

		if (!solutionExists)
		{
			cout << "NO" << endl;
			return;
		}
		cout << "YES" << endl << roads.size() / 2 << endl;
		for (int i = 0; i < roads.size(); i += 2)
			std::cout << roads[i] << " " << roads[i + 1] << std::endl;

	}
private:

	vector<vector<Edge>> edges;
	void __buildEdge__(City a, City b, vector <Bar>&bar) {
		bool ableToBuild = true;
		for (int i = 0; ableToBuild && i < bar.size(); ++i)
			ableToBuild = !intersect(a, b, City(bar[i].xs, bar[i].ys, " ", 0), City(bar[i].xe, bar[i].ye, "", 0));

		if (ableToBuild)
		{
			double length = sqrt((double)((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)));
			//edges.insert(Edge(a.id, b.id, length));
			edges[a.id].push_back(Edge(a.id, b.id, length));
			edges[b.id].push_back(Edge(b.id, a.id, length));

		}

	}


};

int main (){
	freopen("imput.txt", "rt", stdin);
	int n, m;
	cin >> n >> m;
	Graph g(n);
	g.buildEdge(m);
	g.prim();
	return 0;
}