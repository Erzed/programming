#include <iostream>
#include <vector>
#include <memory>
#include <list>
#include <fstream>
#include <set>
#include <limits>
#include <map> 
#include <cmath>

#define CTIME INT_MAX

using namespace std;

class M_Point{

public:
	double x, y;
	int time;
	M_Point()
	{}
	M_Point(double a, double b)
		:x(a), y(b)
	{
		time = CTIME;
	}

	double rd(M_Point &other){
		return (other.x - x)*(other.x - x) + (other.y - y)*(other.y - y);
	}

	bool operator!=(const M_Point& other) const{
		return !((other.x == x) && (other.y == y));
	}
};
class Graph{
public:
	Graph(int nan1, vector <M_Point> &c, double R)
		:N(nan1), ct(c)
	{

		list < pair < int, double> > ca;
	//	node.resize(nan1+1);
		
		for (int i = 0; i < N; i++)
			node.push_back(ca);
		int k = 0, m = 0;
		for (vector<M_Point>::iterator i = ct.begin(); i != ct.end(); ++i){
			m = 0;
			for (vector<M_Point>::iterator j = ct.begin(); j != ct.end(); ++j){
				if (*i != *j){
					double cur = (*i).rd(*j);
					if (cur < R*R){
					//	if (!node[k]) node[k].reset(new list<pair<int, double> >);
					//	if (!node[m]) node[m].reset(new list<pair<int, double> >);
						bool was= true;
						for (list<pair<int, double> >::iterator vd = node[k].begin(); vd != node[k].end(); ++vd){
							if ((*vd).first == m)
								was = false;
						}
						if (was) (node[k]).push_back(make_pair(m, cur));
						was = true;
						for (list<pair<int, double> >::iterator vd = node[m].begin(); vd != node[m].end(); ++vd){
							if ((*vd).first == k)
								was = false;
						}
						if (was) 	(node[m]).push_back(make_pair(k, cur));

					}
				}
				m++;
			}
			k++;
		}
	}



	vector<double> dij(){
		int current = 0;
		pt.clear();
		for (int i = 0; i < N; i++){
			pt.push_back(CTIME);
		}
		vector<bool> u;
		for (int i = 0; i < N; i++) u.push_back(false);
		pt[0] = 0;
		for (int i = 0; i < N; i++){
			int v = -1;
			for (int j = 0; j < N; ++j)
				if (!u[j] && (v == -1 || pt[j] < pt[i]))
					v = j;
				if (pt[v] == CTIME) break;
				u[v] = true;
				for (list<pair<int, double> >::iterator it = node[v].begin(); it != node[v].end(); ++it){
					int to = (*it).first;
					double len = (*it).second;
					if (pt[v] + len < pt[to]){
						pt[to] = pt[v] + len;
						//p[to] = v;
					}
				}

		}
		return pt;
	}

private:
	int N;
//	int E;

	vector<M_Point>ct;
	vector<double> pt;

	vector< list<pair<int, double> > > node;
};

int main(){
	freopen("input2.txt", "rt", stdin);

	int a;
	double R;
	cin >> a >> R;
	double x1, x2, y1, y2;
	cin >> x1 >> y1 >> x2 >> y2;


	vector <M_Point> v1;
	vector <M_Point> v2;
//	v1.resize(a - 1);
//	v2.resize(a - 1);
	v1.push_back(M_Point(x1, y1));
	v2.push_back(M_Point(x2, y2));
	for (int i = 0; i < a - 2; i++){
		double x, y;
		cin >> x >> y;
		v1.push_back(M_Point(x, y));
		v2.push_back(M_Point(x, y));
	}

	Graph g1(a - 1, v1, R);
	Graph g2(a - 1, v2, R);

	vector<double> q2 = g2.dij();
	vector<double> q1 = g1.dij();	vector <double> x;

	for (vector<double>::iterator it = q1.begin(); it != q1.end(); ++it){
		cout << *it << " ";
	}
	cout << endl;
	for (vector<double>::iterator it = q2.begin(); it != q2.end(); ++it){
		cout << *it << " ";
	}
	cout << endl;
	
	for (int i = 1; i < a - 1; i++){
		x.push_back(sqrt(q1[i]) + sqrt(q2[i]));
	}
	int i = 1;
	double min = CTIME;
	for (vector<double>::iterator it = x.begin(); it != x.end(); ++it){
		if (v1[i].x < 0.0)
		{
			if (min > *it) min = *it;
		}
		i++;
	}
	cout << (min) << endl;
	return 0;
}