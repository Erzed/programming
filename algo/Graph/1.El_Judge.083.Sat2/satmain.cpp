#include <iostream>
#include <vector>
#include <set>
#include <stack>

using namespace std;

class Graph
{

public:

	int N, M;
	vector < set < int > > links;
	vector < int > ssc;
	vector < pair < int, int > > rules;
	vector < short > values;
	vector < short > clean;

	Graph()
	{
		scanf ("%d%d", &N, &M);
		links.resize(2 * N + 1);
		values.assign(N + 1, -1);
		clean.assign(N + 1, -1);
		for (int i = 0; i < M; ++i)
		{
			int in, out, zero;
			scanf ("%d%d", &in, &out);
			if (out != 0)
			{
				cin >> zero;
				rules.push_back(make_pair(existentver(in), existentver(out)));
				if (clean[islN(existentver(in))] == -1)
				{
					clean[islN(existentver(in))] = 1 - ((existentver(in) - 1) / N);
				}
				else
				{
					if (clean[islN(existentver(in))] != 1 - ((existentver(in) - 1) / N))
					{
						clean[islN(existentver(in))] = -2;
					}
				}
				if (clean[islN(existentver(out))] == -1)
				{
					clean[islN(existentver(out))] = 1 - ((existentver(out) - 1) / N);
				}
				else
				{
					if (clean[islN(existentver(out))] != (1 - (existentver(out) - 1) / N))
					{
						clean[islN(existentver(out))] = -2;
					}
				}
			}
			else
			{
				rules.push_back(make_pair(existentver(in), -1));
				if (clean[islN(existentver(in))] == -1)
				{
					clean[islN(existentver(in))] = 1 - ((existentver(in) - 1) / N);
				}
				else
				{
					if (clean[islN(existentver(in))] != (1 - (existentver(in) - 1) / N))
					{
						clean[islN(existentver(in))] = -2;
					}
				}
			}
		}
		
		if (!assertOnULR())
		{
			is = false;
			cout << "No\n";
		}
		else
		{
			is = true;
		}
	}

	bool is;
	int time;
	int ssc_n;
	stack < int > st;
	vector < int > func;
	vector < int > time_in;

	int existentver(int A)
	{
		return (A > 0) ? A : N - A;
	}

	int modd(int A)
	{
		return ((A - 1 + N) % (2 * N) + 1);
	}

	int islN(int A)
	{
		return (A <= N) ? A : A - N;
	}

	void vertexadd(int in, int out)
	{
		links[in].insert(out);
	}

	bool assertOnULR()
	{
		set < int > proofs;
		bool flag = true;
		for (int i = 1; i <= N; ++i)
		{
			if (clean[i] >= 0)
			{
				values[i] = clean[i];
				proofs.insert(i);
			}
			if (clean[i] == -1)
			{
				values[i] == 0;
			}
		}
		while (flag)
		{
			flag = false;
			for (int i = 0; i < M; ++i)
			{
				if (rules[i].second == -1)
				{
					if (rules[i].first != -1)
					{
						if (rules[i].first <= N)
						{
							if (values[islN(rules[i].first)] == 0)
							{
								return false;
							}
							values[islN(rules[i].first)] = 1;
						}
						else
						{
							if (values[islN(rules[i].first)] == 1)
							{
								return false;
							}
							values[islN(rules[i].first)] = 0;

						}
						flag = true;
						proofs.insert(islN(rules[i].first));
						rules[i] = make_pair(-1, -1);
					}
					continue;
				}
				set < int >::iterator it = proofs.find(islN(rules[i].first));
				if (it != proofs.end())
				{
					if (((values[*it] == 0) && (rules[i].first <= N))
						|| ((values[*it] == 1) && (rules[i].first > N)))
					{
						if (rules[i].second <= N)
						{
							if (values[islN(rules[i].second)] == 0)
							{
								return false;
							}
							values[islN(rules[i].second)] = 1;
						}
						else
						{
							if (values[islN(rules[i].second)] == 1)
							{
								return false;
							}
							values[islN(rules[i].second)] = 0;

						}
						flag = true;
						proofs.insert(islN(rules[i].second));
					}
					rules[i] = make_pair(-1, -1);
					continue;
				}
				it = proofs.find(islN(rules[i].second));
				if (it != proofs.end())
				{
					if (((values[*it] == 0) && (rules[i].second <= N))
						|| ((values[*it] == 1) && (rules[i].second > N)))
					{
						if (rules[i].first <= N)
						{
							if (values[islN(rules[i].first)] == 0)
							{
								return false;
							}
							values[islN(rules[i].first)] = 1;
						}
						else
						{
							if (values[islN(rules[i].first)] == 1)
							{
								return false;
							}
							values[islN(rules[i].first)] = 0;

						}
						flag = true;
						proofs.insert(islN(rules[i].first));
					}
					rules[i] = make_pair(-1, -1);
					continue;
				}
				if (rules[i].first == rules[i].second)
				{
					if (rules[i].first <= N)
					{
						if (values[islN(rules[i].first)] == 0)
						{
							return false;
						}
						values[islN(rules[i].first)] = 1;
					}
					else
					{
						if (values[islN(rules[i].first)] == 1)
						{
							return false;
						}
						values[islN(rules[i].first)] = 0;
					}
					flag = true;
					proofs.insert(islN(rules[i].first));
					rules[i] = make_pair(-1, -1);
					continue;
				}
				if (islN(rules[i].first) == islN(rules[i].second))
				{
					flag = true;
					rules[i] = make_pair(-1, -1);
				}
			}
		}
		return true;
	}





};

void DFS(Graph g, int root)
{
	g.st.push(root);
	g.func[root] = g.time_in[root] = ++g.time;
	for (set < int >::iterator it = g.links[root].begin(); it != g.links[root].end(); ++it)
	{
		if (g.ssc[*it] == 0)
		{
			if (g.time_in[*it] == 0)
			{
				DFS(g, *it);
			}
			g.func[root] = (g.func[root] < g.func[*it] ? g.func[root] : g.func[*it]);
		}
	}

	if (g.func[root] == g.time_in[root])
	{
		++g.ssc_n;
		int u;
		do
		{
			u = g.st.top();
			g.ssc[u] = g.ssc_n;
			g.st.pop();
		} while (u != root);
	}
}


void tarjan(Graph& g)
{
	g.time = 0;
	g.ssc_n = 0;
	g.func.resize(2 * g.N + 1);
	g.time_in.resize(2* g.N + 1);
	g.ssc.resize(2 * g.N + 1);
	for (int i = 1; i < 2 * g.N + 1; ++i)
	{
		g.time_in[i] = 0;
		g.func[i] = 0;
		g.ssc[i] = 0;
	}

	for (int i = 1; i < 2 * g.N + 1; ++i)
	{
		if ((g.ssc[i] == 0) && (g.values[g.islN(i)] == -1))
			DFS(g, i);
	}
}
void sat2(Graph& g)
{
	for (int i = 0; i < g.M; ++i)
	{
		if (g.rules[i].first != -1)
		{
			int aqq = 0;
			g.vertexadd(g.modd(g.existentver(g.rules[i].first)), g.existentver(g.rules[i].second));
			g.vertexadd(g.modd(g.existentver(g.rules[i].second)), g.existentver(g.rules[i].first));
		}
	}

	tarjan(g);

	for (int i = 1; i <=g.N; ++i)
	{
		if ((g.ssc[i] == g.ssc[i + g.N]) && (g.ssc[i] != 0))
		{
			cout << "No\n";
			return;
		}
	}
	cout << "Yes\n";
	for (int i = 1; i <= g.N; ++i)
	{
		if (((g.ssc[i] == 0) && (g.values[i] == 1)) || ((g.ssc[i] != 0) && (g.ssc[i] < g.ssc[i + g.N])))

		{
			cout << i << " ";
		}
	}
	cout << "0";
}


int main()
{
	//freopen ("input.txt" , "rt",stdin);
	Graph g;
	if (g.is) {
		sat2(g);
	}
	return 0;
}


