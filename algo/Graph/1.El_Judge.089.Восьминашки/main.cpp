#include <iostream>
#include <set>
#include <vector>
using namespace std;
class vertex
{
public:
	vector<int> board;
	int r;
	int l;

	int getEdges(vertex* edges){
		int pos = -1;
		for (int i = 0; i < 9; ++i)
		{
			if (board[i] == 0)
			{
				pos = i;
				break;
			}
		}
		int edgeN = 0;


		if (pos % 3 < 2)
		{
			edges[edgeN] = *this;
			edges[edgeN].board[pos] = edges[edgeN].board[pos + 1];
			edges[edgeN].board[pos + 1] = 0;

			++edges[edgeN].r;
			++edgeN;
		}
		if (pos < 6)
		{
			edges[edgeN] = *this;
			edges[edgeN].board[pos] = edges[edgeN].board[pos + 3];
			edges[edgeN].board[pos + 3] = 0;

			++edges[edgeN].r;
			++edgeN;
		}
		if (pos % 3 > 0)
		{
			edges[edgeN] = *this;
			edges[edgeN].board[pos] = edges[edgeN].board[pos - 1];
			edges[edgeN].board[pos - 1] = 0;

			++edges[edgeN].r;
			++edgeN;
		}
		if (pos > 2)
		{
			edges[edgeN] = *this;
			edges[edgeN].board[pos] = edges[edgeN].board[pos - 3];
			edges[edgeN].board[pos - 3] = 0;

			++edges[edgeN].r;
			++edgeN;
		}

		return edgeN;
	}
};

bool operator<(const vertex l, const vertex r) 
{
	if (l.l < r.l)
		return true;
	if (l.l > r.l)
		return false;
	if (l.r > r.r)
		return true;
	if (l.r < r.r)
		return false;
	for (int i = 7; i > -1; --i)
	{
		if (l.board[i] > r.board[i])
			return true;
		if (l.board[i] < r.board[i])
			return false;
	}

	return false;
}



int main()
{
	vertex ver;
	for (int i = 0; i < 9; ++i) {
		int a;
		cin >> a;
		ver.board.push_back(a);
	}
	ver.r = 0;
	ver.l = 0;

	set<vertex> Set;
	Set.insert(ver);
	vertex* edges = new vertex[4];
	while (true)
	{
		int edgeCount = ver.getEdges(edges);
		for (int i = 0; i < edgeCount; ++i)
		{
			edges[i].l = ver.r;
			for (int j = 0; j < 9; ++j)
			{
				if (edges[i].board[j] != 0)
				{
					edges[i].l += abs((edges[i].board[j] - 1) / 3 - j / 3) + abs((edges[i].board[j] - 1) % 3 - j % 3);
				}
			}


			Set.insert(edges[i]);

		}
		set<vertex>::iterator it = Set.begin();
		ver = *it;

		bool isEnd = (it->board[8] == 0);
		if (isEnd)
		{
			for (int i = 0; i < 5; ++i)
			{
				if (it->board[i] != i + 1)
				{
					isEnd = false;
					break;
				}
			}
		}

		if (isEnd && it->board[6] == 7 && it->board[7] == 8)
		{
			cout << "Yes" << endl << it->r << endl; 
			break;
		}
		if (isEnd && it->board[6] == 8 && it->board[7] == 7)
		{
			cout << "No" << endl;
			break;
		}
		Set.erase(it);
	}
}