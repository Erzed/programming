#include <iostream>
#include <vector>
#include <climits>
#include <cmath>

using namespace std;
#include <cmath>
int mlog(int n) {
    int p = 0;
    while ((1 << p) < n) ++p;
    return (1 << p);
}

int mlog2(int k) {
	int p = 0;
	while ((1 << p) < k) ++p;
	return p;
}
/*
    RMQ(minimum)
*/

template<class CT>
class SparseTable {
private:

	std::vector<std::vector <CT> > v;
public:

	SparseTable(int n)
	{
		int logn = mlog(n);
		vector<CT> input(logn, INT_MAX);
		for (int i = 0; i < n; ++i) {
			CT inp;
			cin >> inp;
			input[i] = inp;
		}
		v.push_back(input);
		int k = log((double)logn) + 1;
		for (int i = 1; i <= k; ++i) {
			vector<CT> cur(v[i-1].size() - (1 << (i - 1)), 0);
			v.push_back(cur);
			for (int j = 0; j < v[i].size(); ++j) {
				v[i][j] = min(v[i-1][j], v[i - 1][j + (1  << (i - 1))]);
			}
			
		}
	}

	CT rminq(int i, int j) {
		int k = mlog2(j - i + 1) - 1;
		if (i + 1 == j) {return v[0][i];}
		return min(v[k][i], v[j - (1 << k) + 1][k]);
	}


};
int main() {
//	freopen("input.txt", "rt", stdin);
	int n;
	cin >> n;
	SparseTable<float> a(n);
	
	cin >> n;
	for (int i = 0; i < n; ++i) {
		int q, b;
		scanf("%d%d", &q, &b);
		printf("%d\n", a.rminq(q, b));
	}
	return 0;
}