#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
#define DEFINED 1000000
#define INF INT_MAX
using namespace std;
class Graph {
private:
	int n;
	vector <vector <int> > vec;
public:
	Graph (int N, int m)
		:n(N)
	{
		vec.resize (n);
		for (int i = 0; i < n; ++i) vec[i].resize(n, 0);
		for (int i = 0; i < m; ++i) {
			int a, b, c;
			cin >> a >> b >> c;
			vec [a][b] = c;
		}

	}

	int maxflow (int source, int target) {
		f.resize (n);
		for (int i = 0; i < n; ++i) f[i].resize(n, 0);
		q.resize(n);
		link.resize(n);
		
		int mf = 0;
		int af;
		do {
			af = __findPath__(source, target);
			mf += af;
		}while (af > 0);
		return mf;
	}
private:
	vector <vector <int> > f;
	vector <int> q;
	vector <int>link;
	
	int __findPath__(int source, int target) {
		int qp = 0;
		link[target] = -1;
		int qc = 1;
		q[0] = source;
		vector <int> flow (n, 0);
		int i;
		int cur;
		flow[source] = INF;
		while (link[target] == -1 && qp < qc) {
			cur = q[qp];
			for (i = 0; i < n; ++i) {
				if ((vec[cur][i] - f[cur][i]) > 0 && flow[i] == 0) {
					q[qc] = i;
					qc++;
					link[i] = cur;
					if (vec[cur][i] - f[cur][i] < flow[cur])
						flow[i] = vec[cur][i];
					else
						flow[i] = flow[cur];
				}
			}
			qp++;
		}
		if (link[target] == -1) return 0;
		cur = target;
		while (cur != source) {
			f[link[cur]][cur] += flow[target];
			cur = link[cur];
		}
		return flow[target];
	}
};


int main (){
	int n;
	int m;
	cin >> n >> m;
	Graph it(n, m);
	cout << it.maxflow(0, 5) << endl;
	
	return 0;
}