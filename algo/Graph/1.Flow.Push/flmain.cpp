#include <iostream>
#include <vector>
#include <limits.h>

using namespace std;
/************************************************************************/
/* input.txt:
6
9
3 5 4
0 1 16
1 4 6
0 4 13
2 4 9
1 2 12
3 2 7
2 5 20
4 3 13
*/
/************************************************************************/

class Graph {
public:
	int n;
	Graph () {}
	Graph (int N, int m)
		:n(N)
	{
		vec.resize (n);
		for (int i = 0; i < n; ++i) vec[i].resize(n, 0);
		for (int i = 0; i < m; ++i) {
			int a, b, c;
			cin >> a >> b >> c;
			vec [a][b] = c;
		}

	}

	int maxFlow(int s, int t) {
		int n = vec.size();

		vector<int> h(n); //������ �������
		h[s] = n - 1;

		vector<int> maxh(n);
		vector<int> e(n); //���������� �����
		vector<vector <int> > f(n, vector<int>(n)); //���������

		for (int i = 0; i < n; ++i) {
			f[s][i] = vec[s][i];
			f[i][s] = -f[s][i];
			e[i] = vec[s][i];
		}

		for (int sz = 0;;) {
			if (sz == 0) {
				for (int i = 0; i < n; ++i) {
					if (i != s && i != t && e[i] > 0) {
						if (sz != 0 && h[i] > h[maxh[0]])
							sz = 0;
						maxh[sz++] = i;
					}
				}
			}
			if (sz == 0)
				break;
			while (sz != 0) {
				int i = maxh[sz - 1];
				bool pushed = false;
				for (int j = 0; j < n && e[i] != 0; ++j) {
					if (h[i] == h[j] + 1 && vec[i][j] - f[i][j] > 0) {
						int df = min(vec[i][j] - f[i][j], e[i]);
						f[i][j] += df;
						f[j][i] -= df;
						e[i] -= df;
						e[j] += df;
						if (e[i] == 0)
							--sz;
						pushed = true;
					}
				}
				if (!pushed) {
					h[i] = INT_MAX;
					for (int j = 0; j < n; ++j) {
						if (h[i] > h[j] + 1 && vec[i][j] - f[i][j] > 0) {
							h[i] = h[j] + 1;
						}
					}
					if (h[i] > h[maxh[0]]) {
						sz = 0;
						break;
					}
				}
			}
		}
		int flow = 0;
		for (int i = 0; i < n; i++) {
			flow += f[s][i];
		}
		return flow;
	}

private:
	vector <vector <int> > vec;
};

int main() {
	freopen("input.txt", "rt", stdin);
	int n; int m;
	cin >> n >> m;
	Graph i(n, m);
	cout << i.maxFlow(0, 5) << endl;
	return 0;
}