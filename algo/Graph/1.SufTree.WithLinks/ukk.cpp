#include <string>
#include <iostream>
#include <map>
#define DEFINED -100
using namespace std;

class Node {
public:
	int l, r, par, link;
	std::map<char,int> next;

	Node (int l=0, int r=0, int par=-1)
		: l(l), r(r), par(par), link(-1) {}
	int len()  {  return r - l;  }
	int get (char c, int setter) {
		cout << "here, c: " << c << endl;
		if (!next.count(c))  next.insert(make_pair(c, -1));
		if (setter != DEFINED) next[c] = setter;
		return next[c];
	}
};
class State {
public:
	int v, pos;
	State(){}
	State(int v, int pos) : v(v), pos(pos)  {}
};

class Tree {
public:
	string s;
	int n;
	State ptr;
	Tree (string st) 
		:s(st), n(st.size()){
		sz = 1;
		ptr = State(0, 0);
		for (int i = 0; i < n; ++i)
			__treeExtend__(i);
		int ka;
		ka =1;
	}
private:
	void __treeExtend__ (int pos) {
		for(;;) {
			State nptr = __go__ (ptr, pos, pos+1);
			if (nptr.v != -1) {
				ptr = nptr;
				return;
			}

			int mid = __split__ (ptr);
			int leaf = sz++;
			t[leaf] = Node (pos, n, mid);
			//t[mid].get( s[pos] ) = leaf;
			t[mid].get(s[pos], leaf);
			ptr.v = __getLink__(mid);
			ptr.pos = t[ptr.v].len();
			if (!mid)  break;
		}

	}
	int __getLink__(int v) {
		if (t[v].link != -1)  return t[v].link;
		if (t[v].par == -1)  return 0;
		int to = __getLink__(t[v].par);
		return t[v].link = __split__ (__go__ (State(to,t[to].len()), t[v].l + (t[v].par==0), t[v].r));
	}
	int __split__ (State st) {
		if (st.pos == t[st.v].len())
			return st.v;
		if (st.pos == 0)
			return t[st.v].par;
		Node v = t[st.v];
		int id = sz++;
		t[id] = Node (v.l, v.l+st.pos, v.par);
		//t[v.par].get( s[v.l] ) = id;
		t[v.par].get(s[v.l], id);
		//t[id].get( s[v.l+st.pos] ) = st.v;
		t[id].get(s[v.l+st.pos], st.v);
		t[st.v].par = id;
		t[st.v].l += st.pos;
		return id;
	}

	State __go__ (State st, int l, int r) {
		while (l < r)
			if (st.pos == t[st.v].len()) {
				st = State (t[st.v].get(s[l],DEFINED), 0);
				if (st.v == -1)  return st;
			}
			else {
				if (s[ t[st.v].l + st.pos ] != s[l])
					return State (-1, -1);
				if (r-l < t[st.v].len() - st.pos)
					return State (st.v, st.pos + r-l);
				l += t[st.v].len() - st.pos;
				st.pos = t[st.v].len();
			}
			return st;
	}


	int sz;
	Node t[100];
};



int main () {
	return 0;
}