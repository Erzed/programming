#include <iostream>
#include "graph.h"


#define FILE_NAME "input3.txt"
int main(){
	freopen(FILE_NAME, "rt", stdin);
	int a, b;
	cin >> a >> b;
	Graph mgr(a);

	for (int i = 0; i < b; ++i){
		int x, y, w;
		cin >> x >> y >> w;
		mgr.add_edge(x, y, w);
	}
	int k = 1; // start	 0 < k <= N  (!!!!!!!!!!!!!!!!!!!!)
	
	mgr.bellman_ford(k);
	return 0;
}