#include  <iostream>
#include  <vector>
#include <cstdio>
using namespace std;
int n, m, cnt = 0;
vector <vector <int> > graph;
vector <int> list;
vector <bool> u;
bool flag = true;
void dfs(int k){
	u[k] = true;
	for(int i = 0;i < graph[k].size();i++)
		if(!u[graph[k][i]]) dfs(graph[k][i]);
	if(flag) list.push_back(k);
}
int main(){
	cin>>n>>m;
	graph.resize(n); u.resize(n);
	for(int i = 0;i < m;i++){
		int a,b;
		scanf("%d%d", &a, &b);
		graph[a-1].push_back(b-1);
	}
	u.assign(n,0);
	for(int i = 0;i < n;i++)
		if(!u[i]) 
			dfs(i);
	flag = false;
	u.assign(n, 0);
	for(int i = n - 1;i >= 0; i--)
		if(!u[list[i]]){
			dfs(list[i]);
			cnt++;
		}
	cout << cnt << endl;
	return 0;
}
