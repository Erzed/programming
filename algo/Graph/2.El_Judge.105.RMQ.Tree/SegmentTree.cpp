#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>

using namespace std;

int mlog(int n) {
	int p = 0;
	while ((1 << p) < n) ++p;
	return (1 << p);
}

class SegmentTree {
public:
	SegmentTree(int size) 
		:k(size)
	{
		n = mlog(size);
		data.resize(2 * n, INT_MAX);
		for (int i = 0; i < k; ++i) {
			double a;
			scanf("%lf", &a);
			data[n + i] = a;
		}
		for (int i = n - 1; i > 0; --i) {
			data[i] = min(data[2 * i + 1], data[2 * i]);
		}
	}
	double treeOperationResult(int l, int r) {
		l += (n - 1);
		r += (n - 1);
		double answer = INT_MAX;
		while (l <= r) {
			if (l & 1) {
				answer = min(answer, data[l]);
			}
			if (!(r & 1)) {
				answer = min(answer, data[r]);
			}
			l = (l + 1) / 2;
			r = (r - 1) / 2;
		}

		return answer;
	}


private:
	vector<double> data;
	int k;
	int n;

};

int main() {
	int n;
	cin >> n;

	SegmentTree it(n);
	int m;
	cin >> m;
	for (int i = 0; i < m; ++i) {
		int l, r;
		scanf("%d%d", &l, &r);
		printf("%.6f\n", it.treeOperationResult(l +  1, r));
	}
	return 0;
}


