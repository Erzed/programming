#include "FibonacciHeap.h"
#include <vector>

//Private constructor used in extractMin method. Construct a new heap from children of Node.
FibonacciHeap::FibonacciHeap(shared_ptr<Node> child_list_ptr, unsigned int size) : min(nullptr), size(size) {
	if (this->size > 0) {
		min = child_list_ptr;
		auto cur_ptr = child_list_ptr;
		do {
			cur_ptr->parent = nullptr;
			if (cur_ptr->key < min->key) {
				min = cur_ptr;
			}
			cur_ptr = cur_ptr->right;
		} while (cur_ptr != child_list_ptr);
	}
}

unsigned int FibonacciHeap::getSize() {
	return size;
}

shared_ptr<Node> FibonacciHeap::insert(int k) {
	//initialization of new node
	shared_ptr<Node> n(new Node());
	n->degree = 0;
	n->lostChild = false;
	n->parent = nullptr;
	n->child = nullptr;
	n->key = k;
	//add new node to root list
	if (min == nullptr) {
		n->left = n;
		n->right = n;
	}
	else {
		n->right = min;
		n->left = min->left;
		min->left->right = n;
		min->left = n;
	}
	//update min pointer
	if (min == nullptr || min->key > n->key) {
		min = n;
	}
	//increase the size of heap
	size++;
	return n;
}

void FibonacciHeap::merge(FibonacciHeap& other) {
	if (other.size > 0) {
		//redirect links
		auto tmp = min->right;
		min->right = other.min->right;
		other.min->right->left = min;
		other.min->right = tmp;
		tmp->left = other.min;
		//update min pointer
		if (other.min != nullptr && (min == nullptr || min->key > other.min->key)) {
			min = other.min;
		}
		//update size
		size += other.size;
	}
	other.min = nullptr;
	other.size = 0;
}

int FibonacciHeap::extractMin() {
	auto min_ptr = min;
	if (min_ptr != nullptr) {
		//add all children of minimal node to a new heap
		//and merge it with current one
		this->merge(FibonacciHeap(min_ptr->child, min_ptr->degree));
		//the size after merge will automatically be increased by an amount
		//equal to degree of minimal node. We should decrease it back
		size -= min_ptr->degree;
		//if we extract the last node, we should only set min equal nullptr.
		if (min_ptr->right == min_ptr) {
			min = nullptr;
		}
		//else we cut the min node from root list and consolidate heap.
		else {
			min = min_ptr->right;
			min_ptr->right->left = min_ptr->left;
			min_ptr->left->right = min_ptr->right;
			consolidate();
		}
		//decrease size
		size--;
		return min_ptr->key;
	}
	else {
		throw new out_of_range("Heap is empty");
	}
}

//Restores the properties of binomial heap
void FibonacciHeap::consolidate() {
	auto start_ptr = min;
	auto cur_ptr = start_ptr;
	//vector, each cell of which points to node
	//with degree equal to number of vector cell
	vector<shared_ptr<Node>> deg;
	//fill vector root nodes
	vector<shared_ptr<Node>> root_list;
	do {
		root_list.push_back(cur_ptr);
		cur_ptr = cur_ptr->right;
	} while (cur_ptr != start_ptr);

	//merge nodes with equal degrees
	for (int i = 0; i < root_list.size(); i++) {
		auto x = root_list[i];
		int d = x->degree;
		if (d >= deg.size()) {
			deg.resize(d + 1);
		}
		while (deg[d] != nullptr) {
			auto y = deg[d];
			if (x->key > y->key) {
				auto temp = x;
				x = y;
				y = temp;
			}
			//call linker to make 'y' child of 'x'
			link(y, x);
			deg[d] = nullptr;
			//increasing degree of updated node
			d++;
			if (d == deg.size()) {
				deg.push_back(nullptr);
			}
		}
		deg[d] = x;
	}

	//update min pointer
	min = nullptr;
	for (int i = 0; i < deg.size(); i++) {
		if (deg[i] != nullptr) {
			if (min == nullptr || deg[i]->key < min->key) {
				min = deg[i];
			}
		}
	}
}

//Makes node 'y' the child of node 'x'
void FibonacciHeap::link(shared_ptr<Node> y, shared_ptr<Node> x) {
	y->right->left = y->left;
	y->left->right = y->right;
	y->parent = x;
	if (x->child == nullptr) {
		y->right = y;
		y->left = y;
		x->child = y;
	}
	else {
		y->right = x->child->right;
		y->left = x->child;
		x->child->right->left = y;
		x->child->right = y;
	}
	x->degree++;
	y->lostChild = false;
}

void FibonacciHeap::decreaseKey(shared_ptr<Node> node, int new_key) {
	if (node == nullptr || new_key > node->key) {
		throw new invalid_argument("");
	}
	
	node->key = new_key;
	auto parent = node->parent;
	if (parent != nullptr && node->key < parent->key) {
		cut(node, parent);
		cascanding_cat(parent);
	}
	if (node->key < min->key) {
		min = node;
	}
}

//Cut the child node and add it to root list
void FibonacciHeap::cut(shared_ptr<Node> child, shared_ptr<Node> parent) {
	if (child->right != child) {
		child->right->left = child->left;
		child->left->right = child->right;
		parent->child = child->right;
	}
	else {
		parent->child = nullptr;
	}
	parent->degree--;
	
	child->right = min;
	child->left = min->left;
	min->left->right = child;
	min->left = child;

	child->parent = nullptr;
	child->lostChild = false;
}

//Recoursively cut the nodes if it's necessary
void FibonacciHeap::cascanding_cat(shared_ptr<Node> node) {
	auto parent = node->parent;
	if (parent != nullptr) {
		if (!node->lostChild) {
			node->lostChild = true;
		}
		else {
			cut(node, parent);
			cascanding_cat(node);
		}
	}
}

int main () {
	return 0;
}