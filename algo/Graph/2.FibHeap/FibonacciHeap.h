#include <memory>

using namespace std;

struct Node {
	int key;
	shared_ptr<Node> parent;
	shared_ptr<Node> child;
	shared_ptr<Node> left;
	shared_ptr<Node> right;
	unsigned int degree;
	bool lostChild;
};

class FibonacciHeap {
public:
	FibonacciHeap() : min(nullptr), size(0) {}

	shared_ptr<Node> insert(int k);
	void merge(FibonacciHeap& other);
	int extractMin();
	void decreaseKey(shared_ptr<Node> node, int new_key);
	unsigned int getSize();
private:
	FibonacciHeap(shared_ptr<Node> child_list_ptr, unsigned int size);
	void consolidate();
	void link(shared_ptr<Node> y, shared_ptr<Node> x);
	void cut(shared_ptr<Node> child, shared_ptr<Node> parent);
	void cascanding_cat(shared_ptr<Node> node);

	unsigned int size;
	shared_ptr<Node> min;
};