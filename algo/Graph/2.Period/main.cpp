#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
using namespace std;

vector<int> pref(string s) {
	int n = s.length();
	vector<int> pi(n);
	for (int i = 1; i < n; ++i) {
		int j = pi[i - 1];
		while (j > 0 && s[i] != s[j])
			j = pi[j - 1];
		if (s[i] == s[j])
			++j;
		pi[i] = j;
	}
	return pi;

}

void finder(string str) {
	vector <int> ret = pref(str);
	cout << str.size() - ret[ret.size() - 1];
}

int main() {
	//freopen("input.txt", "rt", stdin)
	string str;
	cin >> str;
	//cout << str << " ";
	finder(str);	
	cout << endl;
	
	return 0;
}