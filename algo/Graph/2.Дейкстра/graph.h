
#include <vector>
#include <memory>
#include <list>
#include <set>
#include <cmath>
#include <climits>
using namespace std;

class Graph{
public:
	Graph(int nan1);
	void add_edge(int nan1, int nan2, int w);
	void print_graph();

	void tarjan();
	//////////////////////////////////////////////////////////////////////////
	void bellman_ford(int s);
	void dijkstra(int s);

	//////////////////////////////////////////////////////////////////////////
	
	
	
	int get_count(); // returns N
	int get_edge_weight(int v1, int v2); // returns W(v1, v2) or NULL if there is no edge between this vertices. v1 in (0, N-1)
private:
	int MAX_EDGE;
	int N;
	vector<shared_ptr<list<pair<int, int> > > > node; // list <ver, weight>
	int strongconnect(vector<int> &s, vector<int> &def, vector<int> &lowlink, multiset<pair<int, int> >& scc, int v, int index);
};