#include <iostream>
#include <vector>
#include <memory>
#include <list>
#include <fstream>
#include <set>
#include <limits>
#include <map> 
using namespace std;
 

class Graph{
public:
	Graph(int nan1, int nan2)
		:N(nan1), E(nan2)
	{
		node.resize(nan1);
		for (int i = 0; i < N; i++) node[i].reset(new list<pair<int, int>>);
	
	}

	void add_edge(int nan1, int nan2, int w){
		if (nan1 >= N) return;
		for (auto it = node[nan1]->begin(); it != node[nan1]->end(); ++it){
			if ((*it).first == nan2) return;
		}
		(node[nan1])->push_back(make_pair(nan2, w));
	}

	void print(int nan1){
		nan1--;
		for (auto it = node[nan1]->begin(); it != node[nan1]->end(); it++){
			cout << (*it).first + 1;
		}
	}

	void Dijkstra(){
		vector<int> result(N, INT_MAX); // first vertex, second mark!!!
		set<pair<int, int>> a;
		 
		a.insert(make_pair(0, 0)); // first mark, second vertex!!!!
		
		while (!a.empty){
			int v = a.begin()->second;
			
			result[v] = a.begin()->first;
			a.erase(a.begin());

			for (auto it = node[v]->begin(); it != node[v]->end(); ++it){
				if ((*it).second + result[v] < result[(*it).first]){
					a.erase(make_pair(result[(*it).first], (*it).first));
					result[(*it).first] = (*it).second + result[v];

				}
			}
		}
	}



private:
	int N;
	int E;

	vector< shared_ptr<list<pair<int, int> >>> node;
};
int main(){
 
	Graph my(5, 5);
	ifstream in("input.txt");
	if (in){
		int n, e;
		in >> n >>  e;
		Graph mgr(n, e);
		for (int i = 0; i < e; i++) {
			int a, b;
			in >> a >> b;
			mgr.add_edge(a - 1, b - 1);
		}
		mgr.Dijkstra();
	}

	return 0;

}