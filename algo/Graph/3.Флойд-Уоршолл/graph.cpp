#include "graph.h"
#include <iostream>
#include <algorithm>
#include <queue> 


void Graph::bellman_ford(int s)
{
	vector<int> d;

	for (int i  = 0; i < N; ++i){
		d.push_back(MAX_EDGE);	
	}
	s--;
	d[s] = 0;
	int j = 0;
	for (int i = 0; i < N - 1; ++i){
		j = 0;
		for (auto it = node.begin(); it != node.end(); ++it){

			for (auto ct = (*it)->begin(); ct != (*it)->end(); ++ct){
				if (d[(*ct).first] > (d[j] + (*ct).second)){
					d[(*ct).first] = d[j] + (*ct).second;
				}

			}
			++j;
		}

	}
	j = 0;


	for (auto it = node.begin(); it != node.end(); ++it, ++j){
		
		for (auto ct = (*it)->begin(); ct != (*it)->end(); ++ct) {
			if (d[ct->first] > (d[j] + ct->second)){
				std::cout << "bellman-ford: graph includes negative cycle" << std::endl;
				return;
			}
		}
	}

	for (auto it = d.begin(); it != d.end(); ++it) {
		if ((*it) == MAX_EDGE) cout << "No way to this vertex" << endl;
		else
			cout << (*it) << endl;
	}


}

class mycomparison
{
	bool reverse;
public:

	bool operator() (const pair<int, int>& lhs, const pair<int, int>&rhs) const
	{
		return rhs.first < lhs.first;
	}
};

void Graph::dijkstra(int s) {
	vector<int> d;
	for (int i = 0; i < N; ++i) {
		d.push_back(MAX_EDGE);
	}
	s--;
	d[s] = 0;
	vector<bool>u(N, false);

	for (int i = 0;i < N; ++i) {
		int v = -1;
		for (int j = 0; j < N; ++j) {
			if (!u[j] && (v == -1 || d[j]< d[v])) {
				v = j;
			}
		}
		if (d[v]==MAX_EDGE) break;
		u[v] = true;
		for (auto j = node[v]->begin(); j != node[v]->end(); ++j) {
			int cest = j->first;
			int len = j->second;
				if (d[v] + len < d[cest]) {
				d[cest] = d[v] + len;
			}
		}
	}


	for (auto it = d.begin(); it != d.end(); ++it) {
		if ((*it) == MAX_EDGE) cout << "No way to this vertex" << endl;
		else
			cout << (*it) << endl;
	}

}

Graph::Graph(int nan1)
	:N(nan1), MAX_EDGE(0)
{
	node.resize(nan1);
	for (int i = 0; i < N; i++) node[i].reset(new list<pair<int, int>>);
}
int Graph::get_count(){
	return N;
}
void Graph::add_edge(int nan1, int nan2, int w){
	nan1--;
	nan2--;
	if (nan1 >= N) return;
	for (auto it = node[nan1]->begin(); it != node[nan1]->end(); ++it){
		if ((*it).first == nan2) return;
	}
	MAX_EDGE += abs(w);
	(node[nan1])->push_back(make_pair(nan2, w));

}

void Graph::print_graph(){
	int p = 0;
	for (auto it = node.begin(); it != node.end(); ++it){
		cout << p << ": ";
		for (auto ct = (*it)->begin(); ct != (*it)->end(); ++ct){
			cout << ct->first << " ";
		}
		p++;
		cout << endl;
	}
}


void Graph::tarjan(){
	int index = 0;
	std::vector<int> s;
	vector<int> def(N, -1);
	vector<int> lowlink;
	multiset<pair<int, int> > scc;
	for (int i = 0; i < N; ++i) lowlink.push_back(i);
	for (int v = 0; v < N; ++v){
		if (def[v] == -1){
			index = strongconnect(s, def, lowlink, scc, v, index);
		}
	}
	int current = 0;
	cout << "sCC:" << endl;
	for (auto it = scc.begin(); it != scc.end(); ++it){
		if ((*it).first == current){
			cout << (*it).second + 1 <<endl;
		}
		else{
			cout << "sCC:" << endl;
			current = (*it).first;
			cout << (*it).second  + 1 << endl;
		}
	}
}
int Graph::get_edge_weight(int v1, int v2){
	if (v1 >= N) return NULL;
	for (auto it = node[v1]->begin(); it != node[v1]->end(); ++it) 
		if ((*it).first == v2) return (*it).second;
	return NULL;
}

bool isInStack(vector<int> &s, int v)
{
	for (auto it = s.begin(); it != s.end(); ++it){
		if (*it == v) return true;
	}
	return false;
}
int Graph::strongconnect(vector<int> &s, vector<int> &def, vector<int> &lowlink, multiset<pair<int, int> >& scc, int v, int index){
	def[v] = index;
	lowlink[v] = index;
	index++;
	s.push_back(v);
	for (auto it = node[v]->begin(); it != node[v]->end(); ++it){
		if (def[(*it).first] == -1){
			index = strongconnect(s, def, lowlink, scc, (*it).first, index);
			lowlink[v] = min(lowlink[v], lowlink[(*it).first]);
		}
		else if (isInStack(s, (*it).first)){
			lowlink[v] = min(lowlink[v], def[(*it).first]);
		}
	}


	if (lowlink[v] == def[v] && s.size() > 0){
		int w = v;
		while (w == v || s.size() > 0){
			w = s[s.size() - 1];
			s.pop_back();
			scc.insert(make_pair(lowlink[w], w));
		}

	}
	return index;
}
int Graph::get_max_edge() {
	return MAX_EDGE;
}

vector<vector<int> > Graph::floyd�warshall() {
	vector<vector<int> > w;

	vector<int> c(N, MAX_EDGE);
	for (int i = 0; i < N; ++i) w.push_back(c);
	int ver = 0;
	for (auto it = node.begin(); it != node.end(); ++it, ++ver) {
		for (auto ct = (*it)->begin(); ct != (*it)->end(); ++ct) {
			w[ver][ct->first] = ct->second;
		}
	}

	for (int k = 0; k < N; ++k)
		for (int i = 0; i < N; ++i)
			for (int j = 0; j < N; ++j)
				w[i][j] = min(w[i][j], w[i][k] + w[k][j]);


	return w;
}
