#include <iostream>
#include "graph.h"

#include <queue>
#define FILE_NAME "input.txt"


int main(){
	freopen(FILE_NAME, "rt", stdin);
	int a, b;
	cin >> a >> b;
	Graph mgr(a);

	for (int i = 0; i < b; ++i){
		int x, y, w;
		cin >> x >> y >> w;
		mgr.add_edge(x, y, w);
	}
	int k = 1; // start	 0 < k <= N  (!!!!!!!!!!!!!!!!!!!!)
	
	vector<vector<int> > w;
	w = mgr.floyd�warshall();
	for (auto it = w.begin(); it != w.end(); ++it) {
		for (auto ct = it->begin(); ct != it->end(); ++ct)
			if (*ct != mgr.get_max_edge()) cout << *ct << " ";
			else cout << "INF ";
		cout << endl;
	}

	return 0;
}