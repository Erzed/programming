#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>
#include <map>
using namespace std;

int mlog(int n) {
	int p = 0;
	while ((1 << p) < n) ++p;
	return (1 << p);
}
pair<int, int>& min1(pair<int, int>& a, pair<int, int>& b) {
	if (a.first != b.first) {
		if (a.first < b.first) {
			return a;
		} else return b;
	} else {
		if (a.second > b.second) return a;
		else return b;
	}
}
class Node {
public:
	int p;
	vector<int> child;
	int step;
	Node(int _p, int _step)
		:p(_p), step(_step)
	{}
};
class Graph {
public:
	int n;
	vector <Node > g;
	vector<bool>used;
	vector<pair<int, int> > step;
	Graph() {
		scanf("%d", &n);
		g.push_back(Node(0, 0));
		for (int i = 0; i < n; ++i){
			int a, b;
			scanf("%d%d", &a, &b);
			g.push_back(Node(a, g[b].step + 1));
			g[b].child.push_back(a);
		}
		n++;
		used.resize(n, false);
		dfs(0);

	}

	void dfs(int v) {
		used[v] = true;
		bool w = false;
		for (vector<int>::iterator it = g[v].child.begin(); it != g[v].child.end(); ++it) {
			if (!used[*it]) {
				dfs(*it);
			}
			if (it + 1 != g[v].child.end()) {
			step.push_back(make_pair(g[v].step, g[v].p));
			w = true;
			}
			
		}
		if (!w) {
			step.push_back(make_pair(g[v].step, v));
		}
	}
	vector<pair<int, int> >& getter() {
		return step;
	}
};

class SegmentTree {
public:
	vector<int> nodes;
	SegmentTree(vector<pair<int, int> > &step) 
		:k(step.size())
	{
		nodes.resize(step.size());
		n = mlog(step.size());
		data.resize(2 * n, make_pair(INT_MAX, INT_MAX));
		for (int i = 0; i < k; ++i) {
			data[n + i] = make_pair(step[i].first, step[i].second);
			nodes[step[i].second] = i;
		}
		for (int i = n - 1; i > 0; --i) {
			data[i] = min1(data[2 * i + 1], data[2 * i]);
		}
	}
	int treeOperationResult(int l1, int r2) {
		if (r2 > l1) swap(l1, r2);
		int l = nodes[l1];
		int r = nodes[r2];
		pair<int, int> answer = make_pair(INT_MAX, INT_MAX);
		if (l > r) swap(l, r);
		l += (n);
		r += (n);
		while (l <= r) {
			if (l & 1) {
				answer = min1(answer, data[l]);
			}
			if (!(r & 1)) {
				answer = min1(answer, data[r]);
			}
			l = (l + 1) / 2;
			r = (r - 1) / 2;
		}

		return answer.second;
	}


private:
	vector<pair<int, int> > data;
	int k;
	int n;

};

int main() {
	//freopen("input.txt", "rt", stdin);
	int n;
	Graph i = Graph();
	SegmentTree it(i.getter());
	int m;
	cin >> m;
	for (int i = 0; i < m; ++i) {
		int l, r;
		scanf("%d%d", &l, &r);
		printf("%d\n", it.treeOperationResult(l, r));
	}
	return 0;
}


