#include <iostream>
#include <stack>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;

bool regAssertion(const string& regex);
int maxLen(const string& regex, const char x);
string excessStartDeletion(string& regex);
bool assertX(vector<string>& v, const char x);
bool assertXInWord(const string& word, const char x);
int wordCount(const string& word, const char x);

int main() {
    //freopen("readMe.txt", "rt", stdin);
    string regex;
    char x;
    cin >> regex >> x;
    if (!regAssertion(regex)) {
        cout << "Regex is not valid." << endl;
        return 1;
    } else{

        if (x != 'a' && x != 'b' && x != 'c') {
            cout << "x is not in alphabet." << endl;
            return 2;
        }
    }
    regex = excessStartDeletion(regex);
    cout << maxLen(regex, x) << endl;
    return 0;
}
string excessStartDeletion(string& regex) {
    for (int i = 0; i < regex.size() - 1; ++i) {
        if (regex[i] == '*' && regex[i + 1] == '*') {
            regex.erase(regex.begin() + i);
        }
    }
    return regex;
}
bool regAssertion(const string& regex) {
    int num_of_subexpressions = 0;
    for (char c : regex) {
        if (isalpha(c) || c == '1') { // for letters & 1
            num_of_subexpressions++;
        } else if (c == '*') { 
            continue;
        } else {    // For '+' and '.'
            num_of_subexpressions--;
        }
    }
    if (num_of_subexpressions != 1) {
        return false;
    } else {
        return true;
    }
}


int maxLen(const string& regex, const char x) {
    stack<vector<string> > st;
    for (char c : regex) {
        switch (c) {
        case '1':
            {
                st.push(vector<string>(1, ""));
                break;
            }
        case '+':
            {
                vector<string> right = st.top();
                st.pop();
                vector<string> left = st.top();
                st.pop();
                vector<string> new_v;
                for (auto x : left) {
                    new_v.push_back(x);
                }
                for (auto x : right) {
                    new_v.push_back(x);
                }
                st.push(new_v);
                break;
            }
        case '.':
            {
                vector<string> right = st.top();
                st.pop();
                vector<string> left = st.top();
                st.pop();
                int lsize = left.size();
                int rsize = right.size();
                vector<string> new_v(rsize * lsize);
                for (int i = 0; i < lsize; ++i) {
                    for (int j = 0; j < rsize; ++j) {
                        new_v[i * rsize + j] = left[i] + right[j];
                    }
                }
                st.push(new_v);
                break;
            }
        case '*':
            {   
                vector<string> v = st.top();
                st.pop();
                if (assertX(v, x)) {
                    cout << "INF" << endl;
                    exit(0);
                }
                vector<string> new_v;
                for (string word : v) {
                    new_v.push_back(word);
                    new_v.push_back(word + word);
                }
                for (int i = 0; i < v.size(); ++i) {
                    for (int j = 0; j < v.size(); ++j) {
                        new_v.push_back(v[i] + v[j]);
                    }
                }
                new_v.push_back("");
                st.push(new_v);
                break;
            }
        default: {
            st.push(vector<string>(1, string(1, c)));
            break;
                 }
        }
    }
    vector<string> v = st.top();
    int k = 0;
    for (string word : v) {
        int word_k = wordCount(word, x);
        if (word_k > k) {
            k = word_k;
        }
    }
    return k;
}
bool assertX(vector<string>& v, const char x) {
    for (string word : v) {
        if (assertXInWord(word, x)) {
            return true;
        }
    }
    return false;
}

bool assertXInWord(const string& word, const char x) {
    for (char c : word) {
        if (c != x) {
            return false;
        }
    }
    return true;
}

int wordCount(const string& word, const char x) {
    int k = 0;
    int count = 0;
    for (char c : word) {
        if (c == x) {
            count++;
        } else {
            if (count > k) {
                k = count;
            }
            count = 0;
        }
    }
    if (count > k) {
        k = count;
    }
    return k;
}