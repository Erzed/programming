#include "graph.h"
#include <iostream>
#include <algorithm>

Graph::Graph(int nan1)
	:N(nan1)
{
	node.resize(nan1);
	for (int i = 0; i < N; i++) node[i].reset(new list<pair<int, int>>);
}

void Graph::add_edge(int nan1, int nan2, int w){
	nan1--;
	nan2--;
	if (nan1 >= N) return;
	for (auto it = node[nan1]->begin(); it != node[nan1]->end(); ++it){
		if ((*it).first == nan2) return;
	}
	(node[nan1])->push_back(make_pair(nan2, w));

}

void Graph::print_graph(){
	int p = 0;
	for (auto it = node.begin(); it != node.end(); ++it){
		cout << p << ": ";
		for (auto ct = (*it)->begin(); ct != (*it)->end(); ++ct){
			cout << ct->first << " ";
		}
		p++;
		cout << endl;
	}
}


void Graph::tarjan(){
	int index = 0;
	std::vector<int> s;
	vector<int> def(N, -1);
	vector<int> lowlink;
	multiset<pair<int, int> > scc;
	for (int i = 0; i < N; ++i) lowlink.push_back(i);
	for (int v = 0; v < N; ++v){
		if (def[v] == -1){
			index = strongconnect(s, def, lowlink, scc, v, index);
		}
	}
	int current = 0;
	cout << "sCC:" << endl;
	for (auto it = scc.begin(); it != scc.end(); ++it){
		if ((*it).first == current){
			cout << (*it).second + 1 <<endl;
		}
		else{
			cout << "sCC:" << endl;
			current = (*it).first;
			cout << (*it).second  + 1 << endl;
		}
	}
}


bool isInStack(vector<int> &s, int v)
{
	for (auto it = s.begin(); it != s.end(); ++it){
		if (*it == v) return true;
	}
	return false;
}
int Graph::strongconnect(vector<int> &s, vector<int> &def, vector<int> &lowlink, multiset<pair<int, int> >& scc, int v, int index){
	def[v] = index;
	lowlink[v] = index;
	index++;
	s.push_back(v);
	for (auto it = node[v]->begin(); it != node[v]->end(); ++it){
		if (def[(*it).first] == -1){
			index = strongconnect(s, def, lowlink, scc, (*it).first, index);
			lowlink[v] = min(lowlink[v], lowlink[(*it).first]);
		}
		else if (isInStack(s, (*it).first)){
			lowlink[v] = min(lowlink[v], def[(*it).first]);
		}
	}


	if (lowlink[v] == def[v] && s.size() > 0){
		int w = v;
		while (w == v || s.size() > 0){
			w = s[s.size() - 1];
			s.pop_back();
			scc.insert(make_pair(lowlink[w], w));
		}

	}
	return index;
}
