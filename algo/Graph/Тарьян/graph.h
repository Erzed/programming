
#include <vector>
#include <memory>
#include <list>
#include <set>

using namespace std;

class Graph{
public:
	Graph(int nan1);
	void add_edge(int nan1, int nan2, int w);
	void print_graph();

	void tarjan();
private:
	int N;
	vector<shared_ptr<list<pair<int, int> > > > node; // list <ver, weight>
	int strongconnect(vector<int> &s, vector<int> &def, vector<int> &lowlink, multiset<pair<int, int> >& scc, int v, int index);
};