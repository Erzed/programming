#include <iostream>
#include "graph.h"


#define FILE_NAME "input.txt"
int main(){
	freopen(FILE_NAME, "rt", stdin);
	int a, b;
	cin >> a >> b;
	Graph mgr(a);
	for (int i = 0; i < b; ++i){
		int x, y;
		cin >> x >> y;
		mgr.add_edge(x, y, 0);
	}
	

	mgr.tarjan();
	return 0;
}